#!/bin/bash

case ${START} in
    authen)
        npm run test-authen
    ;;
    dataRequest)
        npm run test-dataRequest
    ;;
    mock-idp)
        ./scripts/idp-start.sh
    ;;
    mock-as)
        ./scripts/as-start.sh
    ;;
    mock-rp)
        ./scripts/rp-start.sh
esac