#### Getting started

1.  Install dependencies

    ```sh
    npm install
    ```
2.  Run smart contract (tendermint ABCI app) server in smart-contract repository and wait for

    ```sh
    Commit
    Commit
    ```
    to show in an output before starting api process.

3.  Run NDID API server in api repository as role

    ```sh
    RP
    IDP
    AS
    ```

## Run Test
* Test running on the same machine

    Authentication flow (IDP,RP)
    ```sh
    npm run test-authen
    ```

    Data request flow (IDP,RP,AS)
    ```sh
    npm run test-dataRequest
    ```
* Test not running on the same machine

    At IDP node for start IDP mockserver to listen request and response automatically
    ```sh
    cd scripts/
    ./idp-start.sh
    ```
    
    At AS node for start AS mockserver to listen data request and response automatically
    ```sh
    cd scripts/
    ./as-start.sh
    ```

    At RP node for start create request
    ```sh
    cd scripts/
    ./rp-start.sh
    ```

**Environment variable options**

* `IDP_API_IP` : IP address to contact IDP API server. [Default: `localhost`]
* `IDP_API_PORT` : A port to contact IDP API server. [Default: `8080`]
* `RP_API_IP` : IP address to contact RP API server. [Default: `localhost`]
* `RP_API_PORT` : A port to contact RP API server. [Default: `8081`]
* `AS_API_IP` : IP address to contact AS API server. [Default: `localhost`]
* `AS_API_PORT` : A port to contact AS API server. [Default: `8082`]
* `MOCK_SERVER_RP_IP` : IP address of mockserver RP for NDID server to send callback. [Default: `localhost`]
* `MOCK_SERVER_RP_PORT` : A port of mockserver RP for NDID server to send callback. [Default: `1070`]
* `MOCK_SERVER_IDP_IP` : IP address of mockserver IDP for NDID server to send callback. [Default: `localhost`]
* `MOCK_SERVER_IDP_PORT` : A port of mockserver IDP for NDID server to send callback. [Default: `1080`]
* `MOCK_SERVER_AS_IP` : IP address of mockserver AS for NDID server to send callback. [Default: `localhost`]
* `MOCK_SERVER_AS_PORT` : A port of mockserver AS for NDID server to send callback. [Default: `1090`]
* `NS` : Namespace for IDP create identity or for RP create request. [Default: `cid`]
* `ID` : Identifier for IDP create identity or for RP create request. [Default: `1234`]
* `STATUS` : Status for IDP response can be `accept`,`reject`,`random` [Default: `accept`]
* `DATA_REQUEST` : Data request for RP create request can be `yes`,`no` [Default: `no`]
* `MIN_IDP` : min_idp for RP create request can be `Integer` [Default: `1`]
* `MIN_IAL` : min_ial for RP create request or AS register service can be `Float 1.1, 1.2, 1.3, 2.1, 2.2, 2.3, 3` [Default: `1.1`]
* `MIN_AAL` : min_aal for RP create request or AS register service can be `Float 1, 2.1, 2.2, 3` [Default: `1`]
* `IAL` : ial for IDP create response can be `Float 1.1, 1.2, 1.3, 2.1, 2.2, 2.3, 3` [Default: `3`]
* `AAL` : aal for IDP create response can be `Float 1, 2.1, 2.2, 3` [Default: `3`]
* `LARGE_DATA` : Large data for AS response data size 5mb to platform can be `yes`,`no` [Default: `no`]

## Examples

* At IDP node for start IDP mockserver to listen request and response automatically

```sh
cd scripts/
NS=cid ID=1234567890 STATUS=accept ./idp-start.sh
```

* At AS node for start AS mockserver to listen data request and response automatically

```sh
cd scripts/
./as-register-service.sh
./as-start.sh
```

* At RP node for start create request

```sh
cd scripts/
NS=cid ID=1234567890 DATA_REQUEST=yes ./rp-create-request.sh
```
