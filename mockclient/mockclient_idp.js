const express = require("express");
const bodyParser = require("body-parser");
const shell = require("shelljs");
const path = require("path");
const fs = require("fs");
const fetch = require("node-fetch");
const { spawnSync } = require("child_process");
const uuidv1 = require('uuid/v1');
const config = require("../config.js");

const MOCK_SERVER_IDP_IP = process.env.MOCK_SERVER_IDP_IP || "localhost";
const MOCK_SERVER_IDP_PORT = process.env.MOCK_SERVER_IDP_PORT || 1080;
const autoResponse = process.env.AUTO_RESPONSE || "no";
const app = express();

app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: true }));

spawnSync("mkdir", ["-p", "./dev_user_key/"]);

let nonce = uuidv1();

process.on("unhandledRejection", function(reason, p) {
  console.error("Unhandled Rejection:", p, "\nreason:", reason.stack || reason);
});

function _autoResponse() {
  return autoResponse.toLowerCase() === "yes";
}

if (!_autoResponse()) {
  var redis = require("redis");
  var pub = redis.createClient({
    host: config.REDIS_IP,
    port: config.REDIS_PORT
  });
}

if (_autoResponse()) {
  let identityFromFile = fs.readFileSync(
    path.join(__dirname, "..", "features", "idp", "identity.json"),
    "utf8"
  );
  let identity = JSON.parse(identityFromFile);
  identity.forEach(async (element) => {
    try {
      shell.exec(
        `${path.join(
          __dirname,
          "..",
          "scripts",
          "idp-create-identity.sh"
        )} '${element.namespace}' '${element.identifier}' '${element.ial}' '${nonce}'`
      );
    } catch (error) {
      throw error;
    }
  });
}

app.post("/idp/request", (req, res) => {
  const { request } = req.body;
  if (request.type === "onboard_request") { //Result consent for onboard
    try {
      shell.exec(
        `${path.join(
          __dirname,
          "..",
          "scripts",
          "idp-receive-response-onboard.sh"
        )} '${JSON.stringify(request)}'`
      );
    } catch (error) {
      throw error;
    }
  } else {
    if (_autoResponse()) {
      try {
        shell.exec(
          `${path.join(
            __dirname,
            "..",
            "scripts",
            "idp-send-response.sh"
          )} '${JSON.stringify(request)}' '${nonce}'`
        );
      } catch (error) {
        throw error;
      }
    } else {
      pub.publish("callback_from_idp_platform", JSON.stringify(request));
    }
  }
  res.status(200).end();
});

app.listen(MOCK_SERVER_IDP_PORT, () => {
  console.log(`Mock server IDP listen on port ${MOCK_SERVER_IDP_PORT}`);
});
